package ifsc.edu.br.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {
    // variáveis para referenciar os componentes gráficos da tela
    private TextView amountTextView;
    private TextView percentTextView;
    private TextView tipTextView;
    private TextView totalTextView;

    // objetos para formatar os números na interface
    private static NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
    private static NumberFormat percentFormat = NumberFormat.getPercentInstance();

    // variáveis da lógica da aplicação
    private double billAmount = 0.0; // valor da conta inserido pelo usuário
    private double percent = 0.15; // valor da taxa de serviço – 15% por padrão

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Definimos refêrencias para os componentes gráficos do layout

        amountTextView = (TextView) findViewById(R.id.amountTextView);
        percentTextView = (TextView) findViewById(R.id.percentTextView);
        tipTextView = (TextView) findViewById(R.id.tipTextView);
        totalTextView = (TextView) findViewById(R.id.totalTextView);

        // coloca como texto inicial das Views tip e total como 0
        tipTextView.setText(currencyFormat.format(0));
        totalTextView.setText(currencyFormat.format(0));

        //Definindo referencia para o seekbar
        SeekBar percentSeekBar = (SeekBar) findViewById(R.id.percentSeekBar);
        percentSeekBar.setOnSeekBarChangeListener(seekBarListener);


        //Definimos referência para o amountEditText
        EditText amountEditText = (EditText) findViewById(R.id.amountEditText);
        amountEditText.addTextChangedListener (amountEditTextWatcher);

    }

    private void calculate() {
        // formata e mostra a percentagem correta na percentTextView da tela
        percentTextView.setText(percentFormat.format(percent));
        // calcula o valor da gorjeta e o novo valor total
        double tip = billAmount * percent;
        double total = billAmount + tip;
        // formata e mostra os novos valores do troco e total na tela
        tipTextView.setText(currencyFormat.format(tip));
        totalTextView.setText(currencyFormat.format((total)));
    }
    private final SeekBar.OnSeekBarChangeListener seekBarListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            percent =progress/100.0;
            calculate();

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    private final TextWatcher amountEditTextWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s,
                                  int i, int i1, int i2) {
            // tratamento de exceção para prevenção de erros
            try {
                // obtem o valor da conta e mostra em valor de moeda na
                // amountTextView
                billAmount = Double.parseDouble(s.toString()) / 100.0;
                amountTextView.setText(currencyFormat.format(billAmount));
            } catch (NumberFormatException e) {
                // se a entrada for um valor em branco ou não numérico, o cálculo
                // dará errado e disparará a exceção NumberFormatException
                amountTextView.setText(R.string.enter_amount);
                billAmount = 0.0;
            }
            // invoca o método calculate para atualizar a nova percentagem
            // (re) calcular o valor do troco e total
            calculate();
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

        @Override
        public void afterTextChanged(Editable editable) {}
    };

}
